### General Aliaes {{{1

# Dotfiles editing {{{2
alias reload="source ~/.zshrc"
alias ez="$EDITOR ~/.zshrc && reload" # Edit default zsh config file and reload
alias ea="$EDITOR ~/.my-zsh/zsh-config/aliases.zsh && reload" # edit custom aliases and functions config and reload
alias eg="$EDITOR ~/.my-zsh/zsh-config/gitaliases.zsh && reload" # edit custom git aliases and functions config and reload
alias et="$EDITOR ~/.tmux.conf" # edit tmux configuration
alias vimrc="$EDITOR ~/.vimrc" # Edit vimrc

# Finder aliases {{{2
alias ls="ls --color=always"
alias ll="ls -lhFA"
alias l="ls -lhF"
alias l.="ls -dl .[^.]*"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."

alias cdd="cd -" # go to previious directory
alias tad="ta 0" # go to the default tmux session

# Terminal commands {{{2
alias c="clear"
alias clera="clear"
alias cl="clear; l"
# alias h="history" # use zsh plugin history instead

alias e="exit"
alias :q="exit"
alias ,q="exit"
alias qall="tkss work && tkss sys"
alias o="open ."
alias md="mkdir -p"
alias rm="rm -i"

alias t='tail -f'
alias -g H='| head'
alias -g T='| tail'
alias -g G='| grep'
alias -g L='| less'
alias -g M='| most'

alias dud='du -d 1 -h'
alias duf='du -sh *'
alias fd='find . -type d -name'
alias ff='find . -type f -name'
alias p='ps -f'

# Tree Commands {{{2
alias tree='tree -C'
alias t1='tree -L 1' # show directory structure one level
alias t2='tree -L 2' # show directory structure two level
alias t3='tree -L 3' # show directory sturcture 3 level
alias t4='tree -L 4'

# System aliases {{{2
alias shutdown="sudo shutdown -h now"
alias reboot="sudo shutdown -r now"

alias tu="top -o cpu" # cpu
alias tm="top -o vsize" # memory
alias vhttpd="ps aux | grep httpd"
alias vmysql="ps aux | grep mysql"

# Misc. {{{2
alias acache='apt-cache search --names-only'

alias trash='trash-put'
alias sslocal='sslocal -c ~/.config/shadowsocks/config.json'

# tmux {{{2
alias ta='tmux attach -t'
alias ts='tmux new-session -s'
alias tl='tmux list-sessions'
alias tkss='tmux kill-session -t'
### Development aliases {{{1

# NPM {{{2
alias npmlsg="npm list -g --depth=0"
alias npmls="npm list --depth=0"

# Apache {{{2
alias restartapache="sudo apachectl restart"
alias stopapache="sudo apachectl stop"
alias startapache="sudo apachectl start"

# MongoDB
alias startmongo="sudo service mongod start"
alias stopmongo="sudo service mongod stop"
alias restartmongo="sudo service mongod restart"


### Functions: Directory {{{1
# Enter a directory and list its contents {{{2
cdl() {
  cd $1
  clear
  l
}

# create a new directory and cd into that directory {{{2
take() {
  mkdir $1
  cd $1
}

### Functions: Grep {{{1
# grep string $1 in aliases {{{2
aga() {
  alias | grep "$1"
}

# list the directories with the keyword {{{2
lg () {
  clear
  ll | grep -i "$1"
}

# Kill named process {{{2
killnamed() {
  ps ax | grep $1 | cut -d ' ' -f 1 | xargs kill
}

# grep named process {{{2
psg() {
  ps aux | grep $1
}

## Functions: Find {{{1
# Move all files into a specified diretory {{{2
moveall() {
  find . -maxdepth $1 -type f -iname "$2" -print -exec mv {} $3 \;
}

# Show all specified files {{{2
showall() {
  find . -type f \( -iname "*.$1" -or -iname "*.$2" -or -iname "*.$3" -or -iname "*.$4" \) -print
}
# Trash all specified files {{{2
trashall() {
  find . -type f \( -iname "*.$1" -or -iname "*.$2" -or -iname "*.$3" -or -iname "*.$4" \) -exec trash {} \;
}
## Functions: Archives {{{1

# add a zip file {{{2
zir() {
  zip -r $1 $*
}

# add a tgz file {{{2
taz() {
  tar -czvf $1 $*
}

# extract a tgz file {{{2
tax() {
  tar -xzvf $1
}
# extract a special bundle of archives {{{2
unpack() {
  cd $1
  unzip -oq '*.zip' # unarchive zip package to current director
  unrar e -o+ *.rar # extract files without archive path in overwritten mode
}


### Functions: Workflow {{{1
# Create a new file and open it with vim {{{2
new() {
  touch $1
  vim $1
}

