# Installation

1. clone this repo to your home directory as '~/.my-zsh'
2. from home directory root, run `~/.my-zsh/install.sh` to run the install script. This
   will create the dotfile links and clone the git-zsh-prompt to the `.my-zsh`
directory.
