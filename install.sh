#!/usr/bin/env bash
ln -s ~/.my-zsh/dotfiles/vimrc .vimrc
ln -s ~/.my-zsh/dotfiles/zshrc .zshrc
ln -s ~/.my-zsh/dotfiles/gitignore_global .gitignore_global
ln -s ~/.my-zsh/dotfiles/tmux.conf .tmux.conf
ln -s ~/.my-zsh/dotfiles/agignore .agignore

echo "dotfiles链接完毕。"

# echo "从github克隆git-zsh-prompt ......"

# echo

# git clone https://github.com/olivierverdier/zsh-git-prompt.git ~/.my-zsh/zsh-git-prompt && echo "成功克隆git-zsh-prompt ."

echo "现在安装Vim-plug......"

mkdir -p ~/.vim/autoload
wget -P ~/.vim/autoload/ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "安装完毕，请打开Vim, 然后在命令行运行`PlugInstall`来安装各种插件。"
